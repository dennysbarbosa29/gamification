import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:gamefication/ui/controllers/home_screen_controller.dart';
import 'package:gamefication/ui/controllers/versioncheckprovider.dart';
import 'package:gamefication/ui/router.dart';
import 'package:gamefication/utils/app_colors.dart';
import 'package:provider/provider.dart';

import 'package:flutter/cupertino.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        StreamProvider<FirebaseUser>.value(
          value: FirebaseAuth.instance.onAuthStateChanged,
        ),
        ChangeNotifierProvider<VersionCheck>(
          builder: (_) => VersionCheck(),
        ),
        //TODO: Diogo: maybe this isn't the best way to pass the provider to details screens. verify!
        ChangeNotifierProvider<HomeScreenController>(
          builder: (_) => HomeScreenController(),
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Gamefication',
        theme: ThemeData(
          primarySwatch: Colors.blueGrey,
          scaffoldBackgroundColor: AppColors.homeBackgroundColor,
          appBarTheme: AppBarTheme(
            elevation: 0.0,
            color: AppColors.detailsScreensBackground,
            iconTheme: IconThemeData(color: AppColors.defaultGrey),
            textTheme: TextTheme(
              title: TextStyle(
                fontSize: 22,
                color: AppColors.defaultGrey,
              ),
            ),
          ),
          textTheme: TextTheme(
            title: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.w600,
              color: Colors.black,
            ),
            display1: TextStyle(
              fontSize: 32,
              fontWeight: FontWeight.w400,
              color: Colors.black87,
            ),
            body1: TextStyle(
              fontSize: 18.5,
              fontWeight: FontWeight.normal,
              color: Colors.black,
            ),
            body2: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.normal,
              color: Colors.white,
            ),
            headline: TextStyle(
              fontSize: 36,
              fontWeight: FontWeight.bold,
              color: Colors.orange,
            ),
            subhead: TextStyle(
              fontSize: 13,
              fontWeight: FontWeight.w300,
              color: Colors.grey.withOpacity(0.95),
            ),
          ),
          iconTheme: IconThemeData(size: 32.0, color: Colors.black),
        ),
        onGenerateRoute: Router.generateRoute,
        initialRoute: splashRoute,
      ),
    );
  }
}
