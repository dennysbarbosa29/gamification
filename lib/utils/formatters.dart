import 'package:intl/intl.dart';

class Formatters{
  final DateFormat _dateFormatter = DateFormat("dd/MM/yyyy");
  final NumberFormat _longIntFormatter = NumberFormat("#,###");

  String date(DateTime date){
    return _dateFormatter.format(date);
  }

  String intWithDot(int number){
    return _longIntFormatter.format(number).replaceAll(',', '.');
  }
}