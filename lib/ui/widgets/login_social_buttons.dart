import 'package:flutter/material.dart';
import 'package:gamefication/models/user_model.dart';
import 'package:gamefication/ui/router.dart';
import 'package:gamefication/utils/custom_icons.dart';
import 'package:gamefication/utils/dimensions.dart';

class LoginSocialButtons extends StatelessWidget {
  final VoidCallback googleOnPressed;
  final VoidCallback facebookOnPressed;

  const LoginSocialButtons({
    Key key,
    @required this.googleOnPressed,
    @required this.facebookOnPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        FloatingActionButton(
          heroTag: "googleButton",
          backgroundColor: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.asset('assets/images/google_logo.png'),
          ),
          onPressed: this.googleOnPressed,
        ),
        Dimensions.widthSpacer(Dimensions.screenWidth(context) / 15),
        FloatingActionButton(
          heroTag: "facebookButton",
          backgroundColor: Colors.blue,
          child: Icon(
            CustomIcons.facebook_official,
            color: Colors.white.withOpacity(0.95),
          ),
          onPressed: this.facebookOnPressed,
        ),

        //TODO: Uncomment lines below for debugging
        Dimensions.widthSpacer(Dimensions.screenWidth(context) / 15),
        FloatingActionButton(
          heroTag: "debugButton",
          backgroundColor: Colors.orange[800],
          child: Icon(
            Icons.bug_report,
            color: Colors.grey[700],
          ),
          onPressed: (){User.displayName = 'Joaozinho do Modo Debug ';
                            Navigator.of(context).pushReplacementNamed(homeRoute);},
        ),
        //
      ],
    );
  }
}
