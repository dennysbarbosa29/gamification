import 'package:flutter/material.dart';
import 'package:gamefication/models/reward_model.dart';
import 'slidable_tile.dart';

class RenderTiles extends StatelessWidget {
  final Map<dynamic, dynamic> content;
  final TileType tileType;
  RenderTiles({
    Key key,
    @required this.content,
    @required this.tileType,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    if (content.isNotEmpty) {
      List<Widget> slidables = [];

      switch (tileType) {
        case TileType.Rewards:
          {
            content.forEach((mapName, tileItem) {
              slidables.add(SlidableTile(
                price: tileItem.price,
                tileTitle: tileItem.name,
                tileDescription: tileItem.description,
                tileType: tileType,
                maxRedemptions: tileItem.maxRedemptions,
                currentRedemptions: tileItem.currentRedemptions,
                iconColor: tileItem.rewardStatus == RewardStatus.canBuy
                    ? Colors.green
                    : tileItem.rewardStatus == RewardStatus.notEnoughtPoints
                        ? Colors.blue
                        : null,
              ));
            });
          }
          break;

        case TileType.Purchased:
          {
            content.forEach((mapName, tileItem) {
              slidables.add(SlidableTile(
                price: tileItem.price,
                tileTitle: tileItem.name,
                tileDescription: tileItem.description,
                tileType: tileType,
                date: tileItem.date,
              ));
            });
          }
          break;

        case TileType.Challenge:
          {
            content.forEach((mapName, tileItem) {
              slidables.add(SlidableTile(
                price: tileItem.price,
                tileTitle: tileItem.name,
                tileDescription: tileItem.description,
                tileType: tileType,
                maxRedemptions: tileItem.challengeGoal,
                currentRedemptions: tileItem.challengeProgress,
              ));
            });
          }
          break;
      }

      return Column(
        children: slidables,
      );
    } else
      return Container(
        padding: EdgeInsets.only(top: 20),
        child: Text(
          'Nothing to show here', //TODO: Diogo: Change this to something more pleasant
          textAlign: TextAlign.center,
        ),
      );
  }
}
