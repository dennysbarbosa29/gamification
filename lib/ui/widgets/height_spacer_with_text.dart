import 'package:flutter/material.dart';
import 'package:gamefication/utils/dimensions.dart';

class HeightSpacerWithText extends StatelessWidget {
  final double lineThickness;
  final String text;
  final double textSize;
  final Color textColor;
  const HeightSpacerWithText({
    Key key,
    this.lineThickness = 3.0,
    @required this.text,
    this.textSize,
    this.textColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Dimensions.widthSpacer(80),
        Expanded(
          child: Divider(
            thickness: this.lineThickness,
          ),
        ),
        Dimensions.widthSpacer(),
        Text(
          this.text,
          style: Theme.of(context).textTheme.body1.copyWith(
                fontSize: this.textSize,
                color: textColor,
              ),
        ),
        Dimensions.widthSpacer(),
        Expanded(
          child: Divider(
            thickness: this.lineThickness,
          ),
        ),
        Dimensions.widthSpacer(80),
      ],
    );
  }
}
