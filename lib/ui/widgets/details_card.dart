import 'package:flutter/material.dart';
import 'package:gamefication/utils/dimensions.dart';
import 'package:gamefication/utils/formatters.dart';

enum CardType { Points, Mission }

class DetailsCard extends StatelessWidget {
  final CardType cardType;
  final int firstBalloonContent;
  final int secondBalloonContent;
  final int thirdBalloonContent;
  const DetailsCard(
      {Key key,
      @required this.cardType,
      @required this.firstBalloonContent,
      @required this.secondBalloonContent,
      @required this.thirdBalloonContent})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color baloonColor = Colors.grey[200];
    String cardTitle;
    String label1;
    String label2;
    String label3;
    if (cardType == CardType.Points) {
      cardTitle = 'Points';
      label1 = 'Ranking';
      label2 = 'My Points';
      label3 = 'Total';
    } else {
      if (cardType == CardType.Mission) {
        cardTitle = 'Mission';
        label1 = 'You';
        label2 = 'Now';
        label3 = 'Goal';
      }
    }

    Widget baloonAndLabel({
      @required String label,
      @required String baloonContent,
      Color contentColor,
    }) {
      return Column(
        children: <Widget>[
          Text(
            label,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 18,
              color: Colors.grey[800],
            ),
          ),
          Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.005),
          Container(
            width: Dimensions.screenWidth(context) / 4.2,
            height: Dimensions.screenWidth(context) / 7,
            decoration: BoxDecoration(
              borderRadius:
                  BorderRadius.circular(Dimensions.screenWidth(context) / 50),
              color: baloonColor,
            ),
            child: Center(
              child: Text(
                baloonContent,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color:
                        contentColor != null ? contentColor : Colors.grey[800],
                    fontSize: 25),
              ),
            ),
          )
        ],
      );
    }

    return Card(
      elevation: Dimensions.cardElevation - 3.5,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  cardTitle,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 27,
                    color: Colors.grey[800],
                  ),
                ),
                cardType == CardType.Mission
                    ? GestureDetector(
                        onTap: () {
                          print('tapped');
                        },
                        child: Container(
                          child: Icon(
                            Icons.info_outline,
                            color: Colors.grey[700],
                          ),
                        ),
                      )
                    : Container(),
              ],
            ),
            Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.03),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                baloonAndLabel(
                    baloonContent: cardType == CardType.Mission
                        ? firstBalloonContent.toString() + '%'
                        : firstBalloonContent.toString() + 'º',
                    label: label1),
                baloonAndLabel(
                    baloonContent: cardType == CardType.Mission
                        ? secondBalloonContent.toString() + '%'
                        : Formatters().intWithDot(secondBalloonContent),
                    label: label2,
                    contentColor: cardType == CardType.Mission
                        ? (secondBalloonContent < thirdBalloonContent
                            ? Colors.red
                            : Colors.green)
                        : null),
                baloonAndLabel(
                    baloonContent: cardType == CardType.Mission
                        ? thirdBalloonContent.toString() + '%'
                        : Formatters().intWithDot(thirdBalloonContent),
                    label: label3),
              ],
            )
          ],
        ),
      ),
    );
  }
}
