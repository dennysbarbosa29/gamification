import 'package:flutter/material.dart';
import 'package:gamefication/utils/dimensions.dart';

class AppCard extends StatelessWidget {
  final Widget child;
  const AppCard({Key key, @required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: Dimensions.cardElevation,
      child: Padding(
        padding: const EdgeInsets.all(8.5),
        child: child,
      ),
    );
  }
}
