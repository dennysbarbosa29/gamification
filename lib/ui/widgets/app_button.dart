import 'package:flutter/material.dart';
import 'package:gamefication/utils/dimensions.dart';

class AppButton extends StatelessWidget {
  final Color buttonColor;
  final String text;
  final Color textColor;
  final Widget prefixIcon;
  final VoidCallback onPressed;
  const AppButton({
    Key key,
    @required this.buttonColor,
    @required this.text,
    this.textColor,
    this.prefixIcon,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Dimensions.buttonHeight,
      child: RaisedButton(
        color: this.buttonColor,
        elevation: 4.0,
        child: Row(
          //? Maybe change this later to a stack?
          children: <Widget>[
            if (prefixIcon != null) this.prefixIcon,
            Expanded(
              child: Text(
                this.text,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.body2.copyWith(
                      color: this.textColor,
                    ),
              ),
            ),
            if (prefixIcon !=
                null) // use this to centralize the text in the row when we have a prefixIcon
              Dimensions.widthSpacer(Theme.of(context).iconTheme.size),
          ],
        ),
        onPressed: this.onPressed,
      ),
    );
  }
}
