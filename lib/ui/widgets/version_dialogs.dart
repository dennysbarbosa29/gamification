import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

///[forceAsk]: Dictates which dialog is going to be presented.
///[forceAsk] = false, if Force Update; [forceAsk] = true, if Ask for Update.
showVersionDialog(
    {@required BuildContext context,
    @required bool forceAsk,
    @required Function updateTap,}) async {

  await showDialog<String>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {

      String title = "Nova Atualização Disponível";
      String message = !forceAsk
          ? "Por favor, atualize o seu aplicativo para continuar acessando os seus serviços."
          : "Deseja atualizar o seu aplicativo para obter os novos recursos?";

      String buttonLabel = "Atualizar";
      String buttonLabelAskLater = "Lembrar Mais Tarde";


      return WillPopScope(
        onWillPop: () async =>
            false, //Overrides the press of back button, so the Dialog becomes really dismissible
        child: AlertDialog(
                title: Text(title),
                content: Text(message),
                actions: <Widget>[
                  FlatButton(
                    child: Text(buttonLabel),
                    onPressed: updateTap,
                  ),
                  //if 'force' is selected aka forceAsk=false, the user is forced to click on 'Update'
                  !forceAsk
                      ? null
                      : FlatButton(
                          child: Text(buttonLabelAskLater),
                          onPressed: () => Navigator.pop(context), //askLaterTap,
                        ),
                ],
              ),
      );
    },
  );
}
