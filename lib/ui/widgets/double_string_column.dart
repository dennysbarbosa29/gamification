import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gamefication/utils/dimensions.dart';

class DoubleStringColumn extends StatelessWidget {
  final String string1;
  final String string2;
  const DoubleStringColumn({
    Key key,
    @required this.string1,
    @required this.string2,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        AutoSizeText(
          string1,
          style: Theme.of(context).textTheme.subhead,
          maxLines: 1,
        ),
        Dimensions.heightSpacer(10.0),
        AutoSizeText(
          string2,
          //style: TextStyle(fontSize: 25),
          maxLines: 1,
          minFontSize:26,
        ),
      ],
    );
  }
}
