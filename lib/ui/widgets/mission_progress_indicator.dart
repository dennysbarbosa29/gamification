import 'package:flutter/material.dart';
import 'package:gamefication/utils/app_colors.dart';

const double markerHeight = 30;
const double markerWidth = 44;
const double markerTailHeight = 10.0;

class MarkerPosition {
  final double progressPosition;
  final Alignment tailAlignment;
  final CrossAxisAlignment columnAlignment;

  MarkerPosition({
    @required this.progressPosition,
    @required this.tailAlignment,
    @required this.columnAlignment,
  });
}

class MissionProgressIndicator extends StatelessWidget {
  final double currentProgress;
  final Color currentProgressColor;
  final double desiredProgress;
  final Color desiredProgressMarkerColor;
  final double goalOfTheDay;
  final Color goalOfTheDayMarkerColor;
  MissionProgressIndicator({
    Key key,
    @required this.desiredProgressMarkerColor,
    @required this.goalOfTheDayMarkerColor,
    this.currentProgressColor = Colors.black,
    @required this.currentProgress,
    @required this.desiredProgress,
    @required this.goalOfTheDay,
  }) : super(key: key);

  MarkerPosition _calculateMarkerPosition({
    @required double markerProgress,
    @required double contextWidth,
    bool isBottomMarker = true,
  }) {
    final double markerProgressWidth = markerProgress * contextWidth;
    double _progressPosition;
    Alignment _tailAlignment;
    CrossAxisAlignment _columnAlingment;

    if (markerProgressWidth < markerWidth / 2) {
      _progressPosition = markerProgressWidth;
      _tailAlignment =
          isBottomMarker ? Alignment.topLeft : Alignment.bottomLeft;
      _columnAlingment = CrossAxisAlignment.start;
    } else if (contextWidth - markerProgressWidth < markerWidth / 2) {
      _progressPosition = markerProgressWidth - markerWidth;
      _tailAlignment =
          isBottomMarker ? Alignment.topRight : Alignment.bottomRight;
      _columnAlingment = CrossAxisAlignment.end;
    } else {
      _progressPosition = markerProgressWidth - markerWidth / 2;
      _tailAlignment =
          isBottomMarker ? Alignment.topCenter : Alignment.bottomCenter;
      _columnAlingment = CrossAxisAlignment.center;
    }

    return MarkerPosition(
      progressPosition: _progressPosition,
      tailAlignment: _tailAlignment,
      columnAlignment: _columnAlingment,
    );
  }

  @override
  Widget build(BuildContext context) {
    const double _widgetHeight = 120;
    const double _linearProgressIndicatorLocation = 60;
    final TextStyle markerTextStyle = TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w600,
      color: Colors.white.withOpacity(0.92),
    );

    return Container(
      height: _widgetHeight,
      width: double.infinity,
      child: LayoutBuilder(builder: (context, constraints) {
        final double _contextWidth = constraints.maxWidth;
        final MarkerPosition currentProgressMarkerPositions =
            _calculateMarkerPosition( // TODO: this could be inside the ProrgessMarker widget constructor
          markerProgress: this.currentProgress,
          contextWidth: _contextWidth,
          isBottomMarker: false,
        );
        final MarkerPosition desiredProgressMarkerPositions =
            _calculateMarkerPosition(
          markerProgress: this.desiredProgress,
          contextWidth: _contextWidth,
        );
        final MarkerPosition goalOfTheDayMarkerPositions =
            _calculateMarkerPosition(
          markerProgress: this.goalOfTheDay,
          contextWidth: _contextWidth,
        );
        return Stack(
          children: <Widget>[
            Positioned(
              top: _linearProgressIndicatorLocation,
              child: SizedBox(
                height: 10,
                width: _contextWidth,
                child: LinearProgressIndicator(
                  backgroundColor: Colors.grey.withOpacity(0.4),
                  valueColor: AlwaysStoppedAnimation<Color>(
                      AppColors.linearPogressIndicator),
                  value: this.currentProgress,
                ),
              ),
            ),
            Positioned(
              //CURRENT PROGRESS MARKER
              left: currentProgressMarkerPositions.progressPosition,
              child: Column(
                crossAxisAlignment:
                    currentProgressMarkerPositions.columnAlignment,
                children: <Widget>[
                  Container(
                    height: 20,
                    width: markerWidth,
                    child: Text(
                      '${(this.currentProgress * 100).truncate()}%',
                      textAlign: TextAlign.center,
                      style: markerTextStyle.copyWith(color: Colors.black),
                    ),
                  ),
                  ProgressMarker(
                    tailAlignment: currentProgressMarkerPositions.tailAlignment,
                    color: this.currentProgressColor,
                    child: Icon(
                      Icons.person,
                      color: Colors.white,
                      size: 25,
                    ),
                  ),
                  Container(
                    height: 20,
                    width: 2,
                    color: this.currentProgressColor,
                  ),
                ],
              ),
            ),
            Positioned(
              //DESIRED PROGRESS MARKER
              top: _linearProgressIndicatorLocation,
              left: desiredProgressMarkerPositions.progressPosition,
              child: Column(
                crossAxisAlignment:
                    desiredProgressMarkerPositions.columnAlignment,
                children: <Widget>[
                  Container(
                    height: 20,
                    width: 2,
                    color: this.desiredProgressMarkerColor,
                  ),
                  ProgressMarker(
                    tailAlignment: desiredProgressMarkerPositions.tailAlignment,
                    color: this.desiredProgressMarkerColor,
                    child: Text(
                      '${(this.desiredProgress * 100).truncate()}%',
                      textAlign: TextAlign.center,
                      style: markerTextStyle,
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              //GOAL OF THE DAY MARKER
              top: _linearProgressIndicatorLocation,
              left: goalOfTheDayMarkerPositions.progressPosition,
              child: Column(
                crossAxisAlignment: goalOfTheDayMarkerPositions.columnAlignment,
                children: <Widget>[
                  Container(
                    height: 20,
                    width: 2,
                    color: this.goalOfTheDayMarkerColor,
                  ),
                  ProgressMarker(
                    tailAlignment: goalOfTheDayMarkerPositions.tailAlignment,
                    color: this.goalOfTheDayMarkerColor,
                    child: Text(
                      '${(this.goalOfTheDay * 100).truncate()}%',
                      textAlign: TextAlign.center,
                      style: markerTextStyle,
                    ),
                  ),
                ],
              ),
            ),
          ],
        );
      }),
    );
  }
}

class ProgressMarker extends StatelessWidget {
  final Color color;
  final bool fillBackground;
  final Widget child;
  final Alignment tailAlignment;
  const ProgressMarker({
    Key key,
    this.color = Colors.black,
    this.fillBackground = true,
    this.tailAlignment = Alignment.bottomCenter,
    @required this.child,
  }) : super(key: key);

  //TODO : if not filled remove a fraction of tail so it simulates a border
  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      foregroundPainter: MarkerTail(
        color: this.color,
        tailAlignment: tailAlignment,
      ),
      child: Container(
        height: markerHeight,
        width: markerWidth,
        padding: const EdgeInsets.all(3.0),
        decoration: BoxDecoration(
          color: this.fillBackground ? this.color : Colors.transparent,
          border: Border.all(
              color: this.color, width: this.fillBackground ? 1.0 : 2.2),
          borderRadius: BorderRadius.all(
            Radius.circular(5.5),
          ),
        ),
        child: this.child,
      ),
    );
  }
}

class MarkerTail extends CustomPainter {
  final Color color;
  final Alignment tailAlignment;
  MarkerTail({
    @required this.color,
    @required this.tailAlignment,
  });

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = this.color;
    var path = Path();
    if (tailAlignment == Alignment.bottomCenter) {
      path.moveTo(size.width * 1 / 4, size.height);
      path.lineTo(size.width * 3 / 4, size.height);
      path.lineTo(size.width / 2, size.height + markerTailHeight);
      path.close();
    } else if (tailAlignment == Alignment.bottomLeft) {
      path.moveTo(0, size.height - 5.0);
      path.lineTo(size.width * 2 / 5, size.height);
      path.lineTo(0, size.height + markerTailHeight);
      path.close();
    } else if (tailAlignment == Alignment.bottomRight) {
      path.moveTo(size.width, size.height - 5.0);
      path.lineTo(size.width * 3 / 5, size.height);
      path.lineTo(size.width, size.height + markerTailHeight);
      path.close();
    } else if (tailAlignment == Alignment.topCenter) {
      path.moveTo(size.width * 1 / 4, 0);
      path.lineTo(size.width * 3 / 4, 0);
      path.lineTo(size.width / 2, -markerTailHeight);
      path.close();
    } else if (tailAlignment == Alignment.topLeft) {
      path.moveTo(0, 5.0);
      path.lineTo(size.width / 4, 0);
      path.lineTo(0, -markerTailHeight);
    } else if (tailAlignment == Alignment.topRight) {
      path.moveTo(size.width, 0);
      path.lineTo(size.width, 5.0);
      path.lineTo(size.width * 3 / 4, 0);
      path.lineTo(size.width, -markerTailHeight);
    }

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
