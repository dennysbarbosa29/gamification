import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:gamefication/utils/dimensions.dart';
import 'package:gamefication/utils/formatters.dart';
import 'package:intl/intl.dart';

enum TileType { Challenge, Purchased, Rewards }

//
//https://flutterawesome.com/a-flutter-implementation-of-slidable-list-item-with-directional-slide-actions/

class SlidableTile extends StatelessWidget {
  final String tileTitle;
  final String tileDescription;
  final TileType tileType;
  final DateTime date;
  final int currentRedemptions;
  final int maxRedemptions;
  final int price;
  final Color iconColor;

  SlidableTile({
    Key key,
    @required this.tileTitle,
    @required this.tileDescription,
    @required this.tileType,
    this.date,
    this.currentRedemptions = 0,
    this.maxRedemptions = 0,
    @required this.price,
    this.iconColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Slidable(
          delegate: SlidableDrawerDelegate(),
          child: Container(
            height: 75,
            width: Dimensions.screenWidth(context),
            color: Colors.white,
            child: ListTile(
              leading: Icon(
                Icons.card_giftcard, //TODO: Diogo: receive icon from somewhere
                size: 35,
                color: iconColor != null ? iconColor : Colors.grey[800], 
              ),
              title: Text(
                tileTitle,
                style: TextStyle(
                    color: Colors.grey[800],
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
              ),
              subtitle: Text(
                tileDescription,
                style: TextStyle(
                  color: Colors.grey[700],
                  fontSize: 14,
                ),
              ),
              trailing: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Text(
                    tileType == TileType.Purchased
                        ? DateFormat('yyyy-MM-dd').format(date.toLocal()).toString()
                        : currentRedemptions.toString() +
                            '/' +
                            maxRedemptions.toString(),
                    textAlign: TextAlign.end,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.grey[700],
                    ),
                  ),
                  Dimensions.heightSpacer(5),
                  Text(
                    Formatters().intWithDot(price) + ' points',                    
                    textAlign: TextAlign.end,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.grey[700],
                    ),
                  )
                ],
              ),
            ),
          ),
          secondaryActions: <Widget>[
            IconSlideAction(
              foregroundColor: Colors.white,
              caption: 'pipipi',
              color: Colors.green[300],
              icon: Icons.info_outline,
              onTap: () {},
            ),
            IconSlideAction(
              foregroundColor: Colors.white,
              caption: 'popopo',
              color: Colors.grey,
              icon: Icons.delete,
              onTap: () {},
            ),
          ],
          // actions: <Widget>[
          //   IconSlideAction(
          //     foregroundColor: Colors.white,
          //     caption: 'pipipi',
          //     color: Colors.grey,
          //     icon: Icons.info_outline,
          //     onTap: () {},
          //   ),
          //   IconSlideAction(
          //     foregroundColor: Colors.white,
          //     caption: 'popopo',
          //     color: Colors.grey,
          //     icon: Icons.delete,
          //     onTap: () {},
          //   ),
          // ],
        ),
        Divider(
          color: Colors.grey[400],
          thickness: 1,
          indent: 10,
          endIndent: 10,
          height: 0,
        )
      ],
    );
  }
}
