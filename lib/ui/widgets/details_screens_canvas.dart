import 'package:flutter/material.dart';
import 'package:gamefication/ui/widgets/app_card.dart';
import 'package:gamefication/utils/app_colors.dart';
import 'package:gamefication/utils/dimensions.dart';

class DetailsScreenCanvas extends StatelessWidget {
  final String screenTitle;
  final Widget cardWidget;
  final Widget bodyWidget;
  final String listTitle;

  const DetailsScreenCanvas({
    Key key,
    @required this.screenTitle,
    @required this.cardWidget,
    @required this.bodyWidget,
    @required this.listTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.detailsScreensBackground,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        centerTitle: true,
        title: Text(
          this.screenTitle,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
        ),
      ),
      body: ListView(
        //mainAxisAlignment: MainAxisAlignment.center,
        //crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[Padding(padding: const EdgeInsets.symmetric(horizontal: 10),child:
          this.cardWidget,),
          Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.02),
          Padding(
            padding: const EdgeInsets.only(left: 20, bottom: 10),
            child: Text(
              listTitle,
              style: TextStyle(fontSize: 25,color:Colors.grey[800],),
              textAlign: TextAlign.left,
            ),
          ),
          Divider(
            color: Colors.grey[400],
            thickness: 1,
            indent: 10,
            endIndent: 10,
            height: 0,
          ),
          this.bodyWidget,
        ],
      ),
    );
  }
}
