import 'package:flutter/material.dart';

import 'package:gamefication/ui/views/home_screen_view.dart';
import 'package:gamefication/ui/views/login_screen_view.dart';
import 'package:gamefication/ui/views/purchase_history_screen_view.dart';
import 'package:gamefication/ui/views/recent_activity_screen_view.dart';
import 'package:gamefication/ui/views/rewards_screen_view.dart';
import 'package:gamefication/ui/views/splash_screen_view.dart';

const String homeRoute = '/';
const String loginRoute = 'login';
const String splashRoute = 'splash';
const String rewardsRoute = 'rewards';
const String recentActivityRoute = 'recentActivity';
const String purchaseHistoryRoute = 'purchaseHistoryRoute';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case homeRoute:
        return MaterialPageRoute(builder: (_) => HomeScreenView());
      case loginRoute:
        return MaterialPageRoute(builder: (_) => LoginScreenView());
      case splashRoute:
        return MaterialPageRoute(builder: (_) => SplashScreenView());
      case rewardsRoute:
        return MaterialPageRoute(builder: (_) => RewardsScreenView());
      case recentActivityRoute:
        return MaterialPageRoute(builder: (_) => RecentActivityScreenView());
      case purchaseHistoryRoute:
        return MaterialPageRoute(builder: (_) => PurchaseHistoryScreenView());
      default:
        return MaterialPageRoute(
          builder: (_) {
            return Scaffold(
              body: Center(
                child: Text('BUG: Rota não definida para ${settings.name}'),
              ),
            );
          },
        );
    }
  }
}
