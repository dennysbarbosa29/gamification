import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:gamefication/models/user_model.dart';
import 'package:gamefication/services/auth.dart';
import 'package:gamefication/services/firebase_database.dart';
import 'package:gamefication/ui/controllers/base_controller.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:gamefication/ui/router.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

enum FormType { Login, SingUp }

class LoginScreenController extends BaseController {
  FormType _formType;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  LoginScreenController() {
    _formType = FormType.Login;
  }

  // Private fields vars
  String _password;

  set email(value) => User.email = value;
  set password(value) => _password = value;
  set name(value) => User.displayName = value;
  set phoneNumber(value) => User.phoneNumber = value;
  set formType(FormType value) {
    _formType = value;
    notifyListeners();
  }

  GlobalKey<FormState> get formKey => _formKey;
  FormType get formType => _formType;

  bool _validateAndSaveFields() {
    final _formState = _formKey.currentState;
    if (_formState.validate()) {
      _formState.save();
      return true;
    } else {
      return false;
    }
  }

  Future<void> googleSignIn(BuildContext context) async {
    GoogleSignIn _googleSignIn = GoogleSignIn(
      scopes: ['email'],
    );
    try {
      final user = await _googleSignIn.signIn();
      final userAuth = await user.authentication;
      final AuthCredential credential = GoogleAuthProvider.getCredential(
          accessToken: userAuth.accessToken, idToken: userAuth.idToken);

      try {
        final userResult =
            await FirebaseAuth.instance.signInWithCredential(credential);

        User.uid = userResult.user.uid;
        User.displayName = userResult.user.displayName;
        User.email = userResult.user.email;
      } on PlatformException {
        print(FirebaseAuth.instance
            .fetchSignInMethodsForEmail(email: user.email));
      }

      DocumentSnapshot snapshot = await FirestoreHandler.getUser(
          departamento: User.departamento, empresa: User.empresa);
      if (snapshot.data == null) {
        User.phoneNumber = '000000000';
        FirestoreHandler.addUser(
            departamento: User.departamento, empresa: User.empresa);
      }
      Navigator.pushNamed(context, homeRoute);
    } catch (error) {
      print(error);
    }
  }

  Future<void> facebookSignIn(BuildContext context) async {
          this.setState(ViewState.Busy);

    final facebookLogin = FacebookLogin();
    final result = await facebookLogin.logIn(['email', 'public_profile']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final token = result.accessToken.token;
        final credential =
            FacebookAuthProvider.getCredential(accessToken: token);

        try {
          final userResult =
              await FirebaseAuth.instance.signInWithCredential(credential);
          User.uid = userResult.user.uid;
          User.displayName = userResult.user.displayName;
          User.email = userResult.user.email;
        } on PlatformException {
          //getfacebookemail -> services
          final graphResponse = await http.get(
              'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=$token');
          final Map<String, dynamic> profile = json.decode(graphResponse.body);
          
          //show loginmethodsdialog(email)
          final List<String> signInMethods = await FirebaseAuth.instance
              .fetchSignInMethodsForEmail(email: profile['email']);
          for (int n=0; n < signInMethods.length; n++) {
            //TODO: DIOGO: Mostrar Dialog com os metodos de login ja registrados pra esse email
            print(signInMethods[n]);
          }
                  this.setState(ViewState.Idle);

        }
        //if auth.result
        DocumentSnapshot snapshot = await FirestoreHandler.getUser(
            departamento: User.departamento, empresa: User.empresa);

        if (snapshot.data == null) {
          User.phoneNumber = '000000000';
          FirestoreHandler.addUser(
              departamento: User.departamento, empresa: User.empresa);
        }
        this.setState(ViewState.Idle);

        Navigator.pushNamed(context, homeRoute);

        break;
      case FacebookLoginStatus.cancelledByUser:
              this.setState(ViewState.Idle);

        break;
      case FacebookLoginStatus.error:
              this.setState(ViewState.Idle);

        print('erro');
        print(FacebookLoginStatus.error.index);
        print(result.errorMessage);

        break;
    }
  }

  Future<AuthenticationResult> handleSingInSingUp() async {
    if (_validateAndSaveFields()) {
      // set view as busy, so it shows a loading indicator
      this.setState(ViewState.Busy);
      if (_formType == FormType.Login) {
        AuthenticationResult authResult =
            await AuthenticationServices.emailSingIn(_password);

        debugPrint(authResult.status.toString());
        debugPrint(authResult.errorCode.toString());
        debugPrint(authResult.errorMessage.toString());

        if (authResult.status) {
          DocumentSnapshot snapshot = await FirestoreHandler.getUser(
              departamento: User.departamento, empresa: User.empresa);

          debugPrint(snapshot.data['displayName']);
        }

        this.setState(ViewState.Idle);
        return authResult;
      } else if (_formType == FormType.SingUp) {
        AuthenticationResult authResult =
            await AuthenticationServices.emailSingUp(_password);
        this.setState(ViewState.Idle);
        return authResult;
      }
    }
    return null;
  }
}
