import 'package:flutter/material.dart';
import 'package:gamefication/ui/controllers/base_controller.dart';
import 'package:gamefication/ui/router.dart';
import 'package:gamefication/ui/controllers/login_screen_controller.dart';
import 'package:gamefication/ui/views/base_view.dart';
import 'package:gamefication/ui/widgets/height_spacer_with_text.dart';
import 'package:gamefication/ui/widgets/login_singup_field.dart';
import 'package:gamefication/ui/widgets/login_singup_background.dart';
import 'package:gamefication/ui/widgets/login_social_buttons.dart';
import 'package:gamefication/utils/dimensions.dart';
import 'package:gamefication/utils/validators.dart';

class LoginScreenView extends StatefulWidget {
  LoginScreenView({Key key}) : super(key: key);

  @override
  _LoginScreenViewState createState() => _LoginScreenViewState();
}

class _LoginScreenViewState extends State<LoginScreenView> {
  FocusNode _emailFocus;
  FocusNode _passwordFocus;
  FocusNode _nameFocus;
  FocusNode _phoneNumberFocus;
  final TextStyle style = TextStyle(fontSize: 20.0);

  @override
  void initState() {
    _emailFocus = FocusNode();
    _passwordFocus = FocusNode();
    _nameFocus = FocusNode();
    _phoneNumberFocus = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    _emailFocus.dispose();
    _passwordFocus.dispose();
    _nameFocus.dispose();
    _phoneNumberFocus.dispose();
    super.dispose();
  }

  void _singInSingUp(LoginScreenController loginScreenController) {
    loginScreenController.handleSingInSingUp().then(
      (result) {
        if (result == null)
          return null;
        else if (result.status)
          Navigator.pushNamed(context, homeRoute);
        else if (!result.status) //FIXME : Snackbar not fiding context
          debugPrint(result.errorCode);
        debugPrint(result.errorMessage);
      },
    );
  }

  List<Widget> _buildInputs(
      LoginScreenController loginScreenController, BuildContext context) {
    List<Widget> emailPasswordInputs = [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: LoginSingUpField(
          hintText: 'Email',
          keyboardType: TextInputType.emailAddress,
          textInputAction: TextInputAction.next,
          validator: FieldValidators.validateEmail,
          onSaved: (value) => loginScreenController.email = value,
          focusNode: _emailFocus,
          style: style,
          onFieldSubmitted: (_) {
            FocusScope.of(context).unfocus();
            FocusScope.of(context).requestFocus(_passwordFocus);
          },
        ),
      ),
      Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.01),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: LoginSingUpField(
          isPasswordField: true,
          hintText: 'Senha',
          keyboardType: TextInputType.visiblePassword,
          textInputAction: TextInputAction.done,
          validator: FieldValidators.validatePwd,
          onSaved: (value) => loginScreenController.password = value,
          focusNode: _passwordFocus,
          style: style,
          onFieldSubmitted: (_) {
            FocusScope.of(context).unfocus();
            if (loginScreenController.formType == FormType.Login)
              _singInSingUp(loginScreenController);
            else
              FocusScope.of(context).requestFocus(_nameFocus);
          },
        ),
      ),
    ];

    List<Widget> signUpInputs = [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: LoginSingUpField(
          hintText: 'Nome',
          keyboardType: TextInputType.text,
          textInputAction: TextInputAction.next,
          onSaved: (value) => loginScreenController.name = value,
          focusNode: _nameFocus,
          style: style,
          onFieldSubmitted: (_) {
            FocusScope.of(context).unfocus();
            FocusScope.of(context).requestFocus(_phoneNumberFocus);
          },
        ),
      ),
      Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.01),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: LoginSingUpField(
          hintText: 'Número Celular',
          keyboardType: TextInputType.number,
          textInputAction: TextInputAction.done,
          onSaved: (value) => loginScreenController.phoneNumber = value,
          focusNode: _phoneNumberFocus,
          style: style,
          onFieldSubmitted: (_) {
            FocusScope.of(context).unfocus();
            _singInSingUp(loginScreenController);
          },
        ),
      ),
    ];

    return loginScreenController.formType == FormType.Login
        ? emailPasswordInputs
        : [
            ...emailPasswordInputs,
            Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.01),
            ...signUpInputs
          ];
  }

  List<Widget> _buildButtons(
      LoginScreenController loginScreenController, BuildContext context) {
    Material signInButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(8.0),
      color: Colors.blue, //TODO: Diogo: use themedata buttoncolor
      child: MaterialButton(
        padding: EdgeInsets.symmetric(horizontal: 20),
        onPressed: () async {
          FocusScope.of(context).unfocus();
          _singInSingUp(loginScreenController);
        },
        child: loginScreenController.state == ViewState.Idle
            ? Text("Entrar",
                textAlign: TextAlign.center,
                style: style.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold))
            : SizedBox(
                height: Dimensions.screenHeight(context) * 0.04,
                width: Dimensions.screenHeight(context) * 0.04,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                ),
              ),
      ),
    );

    Material signUpButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(8.0),
      color: Colors.blue, //TODO: Diogo: use themedata buttoncolor
      child: MaterialButton(
        padding: EdgeInsets.symmetric(horizontal: 20),
        onPressed: () async {
          FocusScope.of(context).unfocus();
          _singInSingUp(loginScreenController);
        },
        child: loginScreenController.state == ViewState.Idle
            ? Text("Cadastrar",
                textAlign: TextAlign.center,
                style: style.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold))
            : SizedBox(
                height: Dimensions.screenHeight(context) * 0.04,
                width: Dimensions.screenHeight(context) * 0.04,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                ),
              ),
      ),
    );

    Material backButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(8.0),
      color: Colors.grey[600], //TODO: Diogo: use themedata buttoncolor
      child: MaterialButton(
        padding: EdgeInsets.symmetric(horizontal: 20),
        onPressed: () async {
          FocusScope.of(context).unfocus();
          loginScreenController.formType = FormType.Login;
        },
        child: Text("Voltar",
            textAlign: TextAlign.center,
            style: style.copyWith(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            )),
      ),
    );

    Material newAccButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(8.0),
      color: Colors.grey[600], //TODO: Diogo: use themedata
      child: MaterialButton(
        padding: EdgeInsets.symmetric(horizontal: 20),
        onPressed: () async {
          FocusScope.of(context).unfocus();
          loginScreenController.formType = FormType.SingUp;
        },
        child: Text("Criar Nova Conta",
            textAlign: TextAlign.center,
            style: style.copyWith(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            )),
      ),
    );

    if (loginScreenController.formType == FormType.Login)
      return [
        //Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.02),
        Container(
          height: Dimensions.screenHeight(context) * 0.09,
          width: Dimensions.screenWidth(context) / 0.15,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: signInButton,
              )
            ],
          ),
        ),
        Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.005),
        Container(
          height: Dimensions.screenHeight(context) * 0.09,
          width: Dimensions.screenWidth(context) / 0.15,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: newAccButton,
              )
            ],
          ),
        ),
        Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.02),
        HeightSpacerWithText(
          text: 'Ou',
          textSize: 17,
        ),
        Dimensions.heightSpacer(Dimensions.inBetweenItensPadding * 2),
        Padding(
          padding: const EdgeInsets.only(bottom: 15.0),
          child: LoginSocialButtons(
            googleOnPressed: () => loginScreenController.googleSignIn(context),
            facebookOnPressed: () {
              print('apertou');
              loginScreenController.facebookSignIn(context);
            },
          ),
        ),
      ];
    else
      return [
        Container(
          height: Dimensions.screenHeight(context) * 0.09,
          width: Dimensions.screenWidth(context) / 0.15,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: signUpButton,
              )
            ],
          ),
        ),
        Dimensions.heightSpacer(Dimensions.screenHeight(context) * 0.04),
        Container(
          height: Dimensions.screenHeight(context) * 0.09,
          width: Dimensions.screenWidth(context) / 0.15,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: backButton,
              )
            ],
          ),
        ),
      ];
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<LoginScreenController>(
      controller: LoginScreenController(),
      builder: (context, LoginScreenController loginScreenController, _) {
        return LoginSingUpBackground(
          child: Scaffold(
            backgroundColor: Colors.white,
            body: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.only(
                    top: Dimensions.toppadding,
                    left: Dimensions.sidePadding,
                    right: Dimensions.sidePadding),
                child: Form(
                  key: loginScreenController.formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image(
                        fit: BoxFit.contain,
                        image: AssetImage(
                          'assets/images/logo_open_labs.png',
                        ),
                      ),
                      Dimensions.heightSpacer(
                          Dimensions.inBetweenItensPadding * 2.5),
                      Card(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Column(
                          children: <Widget>[
                            Container(
                              height: Dimensions.screenHeight(context) * 0.06,
                              width: Dimensions.screenWidth(context),
                              decoration: BoxDecoration(
                                color: Colors.grey[200],
                                shape: BoxShape.rectangle,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(8),
                                    topRight: Radius.circular(8)),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: FittedBox(
                                    fit: BoxFit.fitHeight,
                                    child: Text(
                                      loginScreenController.formType ==
                                              FormType.Login
                                          ? 'Login'
                                          : 'Cadastrar',
                                      style: style.copyWith(
                                          color: Colors.black54,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ),
                              //color: Colors.grey,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 8, right: 8),
                              child: SizedBox(
                                  height:
                                      Dimensions.screenHeight(context) * 0.01),
                            ),
                            Column(
                              children:
                                  _buildInputs(loginScreenController, context),
                            ),
                            loginScreenController.formType == FormType.Login
                                ? Container(
                                    height:
                                        Dimensions.screenHeight(context) * 0.05,
                                    width:
                                        Dimensions.screenWidth(context) / 0.12,
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: <Widget>[
                                          Padding(
                                            padding: EdgeInsets.only(right: 8),
                                            child: FlatButton(
                                                textColor: Colors.blue,
                                                onPressed: () {},
                                                child: Text(
                                                    "Esqueceu sua Senha?",
                                                    textAlign: TextAlign.end)),
                                          ),
                                        ]),
                                  )
                                : Dimensions.heightSpacer(
                                    Dimensions.screenHeight(context) * 0.01),
                          ],
                        ),
                      ),
                      ..._buildButtons(loginScreenController, context),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
