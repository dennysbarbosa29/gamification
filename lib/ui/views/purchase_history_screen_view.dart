import 'package:flutter/material.dart';
import 'package:gamefication/ui/controllers/home_screen_controller.dart';
import 'package:gamefication/ui/controllers/purchase_history_screen_controller.dart';
import 'package:gamefication/ui/views/base_view.dart';
import 'package:gamefication/ui/widgets/details_card.dart';
import 'package:gamefication/ui/widgets/details_screens_canvas.dart';
import 'package:gamefication/ui/widgets/slidable_tile.dart';
import 'package:gamefication/ui/widgets/slidable_tiles_renderer.dart';
import 'package:provider/provider.dart';

class PurchaseHistoryScreenView extends StatelessWidget {
  const PurchaseHistoryScreenView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const String _appBarTitle = 'Purchase History';
    return Consumer<HomeScreenController>(
        builder: (context, HomeScreenController homeScreenController, _) {
      return DetailsScreenCanvas(
        screenTitle: _appBarTitle,
        cardWidget: DetailsCard(
          cardType: CardType.Points,
          firstBalloonContent: homeScreenController.selectedGame.ranking,
          secondBalloonContent:
              homeScreenController.selectedGame.currentRewardPoints,
          thirdBalloonContent:
              homeScreenController.selectedGame.totalRewardPoints,
        ),
        listTitle: 'Purchased',
        bodyWidget: RenderTiles(
          content: homeScreenController.selectedGame.purchaseHistory,
          tileType: TileType.Purchased,
        ),
        // bodyWidget: Column(
        //   children: <Widget>[
        //     SlidableTile(
        //       tileTitle: 'Spotify Card',
        //       tileDescription: 'Gift Card Spotify R\$20',
        //       tileType: TileType.Purchased,
        //       date: '2019-09-27',
        //       pointsCost: 500,
        //     ),
        //     SlidableTile(
        //       tileTitle: 'Nespresso',
        //       tileDescription: 'Gift 1 pack coffee Nespresso',
        //       tileType: TileType.Purchased,
        //       date: '2019-10-01',
        //       pointsCost: 650,
        //     ),
        //   ],
        // ),
      );
    });
  }
}
