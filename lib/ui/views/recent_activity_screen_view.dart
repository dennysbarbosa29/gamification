import 'package:flutter/material.dart';
import 'package:gamefication/ui/controllers/home_screen_controller.dart';
import 'package:gamefication/ui/controllers/recent_activity_screen_controller.dart';
import 'package:gamefication/ui/views/base_view.dart';
import 'package:gamefication/ui/widgets/details_card.dart';
import 'package:gamefication/ui/widgets/details_screens_canvas.dart';
import 'package:gamefication/ui/widgets/slidable_tile.dart';
import 'package:gamefication/ui/widgets/slidable_tiles_renderer.dart';
import 'package:provider/provider.dart';

class RecentActivityScreenView extends StatelessWidget {
  const RecentActivityScreenView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const String _appBarTitle = 'My recent activity';
    return Consumer<HomeScreenController>(
      builder: (context, HomeScreenController homeScreenController, _) {
        int currentProgress =
            (homeScreenController.selectedGame.mission.currentProgress * 100)
                .truncate();
        int desiredProgress =
            (homeScreenController.selectedGame.mission.desiredProgress * 100)
                .truncate();
        int goalOfTheDay =
            (homeScreenController.selectedGame.mission.goalOfTheDay * 100)
                .truncate();

        return DetailsScreenCanvas(
          screenTitle: _appBarTitle,
          cardWidget: DetailsCard(
            cardType: CardType.Mission,
            firstBalloonContent: currentProgress,
            secondBalloonContent: desiredProgress,
            thirdBalloonContent: goalOfTheDay,
          ),
          listTitle: 'Challenges',
          bodyWidget: RenderTiles(
            content: homeScreenController.selectedGame.challenges,
            tileType: TileType.Challenge,
          ),
          // bodyWidget: Column(
          //   children: <Widget>[
          //     SlidableTile(
          //       tileTitle: 'Sell Recharges R\$50,00',
          //       tileDescription: 'Sell 30 mobile recharges for R\$50 | 02/Oct',
          //       tileType: TileType.Rewards,
          //       pointsCost: 300,
          //       alreadyTaken: 12,
          //       maxRedemptions: 30,
          //     ),
          //     SlidableTile(
          //       tileTitle: 'Sell Recharges R\$35,00',
          //       tileDescription: 'Sell 10 mobile recharges for R\$35 | 02/Oct',
          //       tileType: TileType.Rewards,
          //       pointsCost: 217,
          //       alreadyTaken: 10,
          //       maxRedemptions: 10,
          //     ),
          //     SlidableTile(
          //       tileTitle: 'Sell Recharges R\$10,00',
          //       tileDescription: '137 points | 02/Oct',
          //       tileType: TileType.Rewards,
          //       pointsCost: 137,
          //       alreadyTaken: 10,
          //       maxRedemptions: 10,
          //     ),
          //     SlidableTile(
          //       tileTitle: 'Sell Recharges R\$10,00',
          //       tileDescription: '175 points | 02/Oct',
          //       tileType: TileType.Rewards,
          //       pointsCost: 500,
          //       alreadyTaken: 15,
          //       maxRedemptions: 40,
          //     ),
          //   ],
          // ),
        );
      },
    );
  }
}
