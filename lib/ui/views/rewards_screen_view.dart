import 'package:flutter/material.dart';
import 'package:gamefication/ui/controllers/home_screen_controller.dart';
import 'package:gamefication/ui/controllers/rewards_screen_controller.dart';
import 'package:gamefication/ui/views/base_view.dart';
import 'package:gamefication/ui/widgets/details_card.dart';
import 'package:gamefication/ui/widgets/details_screens_canvas.dart';
import 'package:gamefication/ui/widgets/slidable_tile.dart';
import 'package:gamefication/ui/widgets/slidable_tiles_renderer.dart';
import 'package:provider/provider.dart';

class RewardsScreenView extends StatelessWidget {
  RewardsScreenView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const String _appBarTitle = 'Rewards';
    return Consumer<HomeScreenController>(
        builder: (context, HomeScreenController homeScreenController, _) {
          return DetailsScreenCanvas(
            screenTitle: _appBarTitle,
            cardWidget: DetailsCard(
              cardType: CardType.Points,
              firstBalloonContent: homeScreenController.selectedGame.ranking,
              secondBalloonContent:
                  homeScreenController.selectedGame.currentRewardPoints,
              thirdBalloonContent:
                  homeScreenController.selectedGame.totalRewardPoints,
            ),
            listTitle: 'Rewards',
            bodyWidget: RenderTiles(
              content: homeScreenController.selectedGame.rewardsMap,
              tileType: TileType.Rewards,
            ),
            // bodyWidget: Column(
            //   children: <Widget>[
            //     SlidableTile(
            //       tileTitle: 'Netflix Card',
            //       tileDescription: 'Gift Card Spotify R\$65',
            //       tileType: TileType.Rewards,
            //       pointsCost: 1200,
            //       alreadyTaken: 3,
            //       maxRedemptions: 10,
            //     ),
            //     SlidableTile(
            //       tileTitle: 'Nike Card',
            //       tileDescription: 'Gift Card Spotify R\$65',
            //       tileType: TileType.Rewards,
            //       pointsCost: 7132,
            //       alreadyTaken: 2,
            //       maxRedemptions: 5,
            //     ),
            //     SlidableTile(
            //       tileTitle: 'Spotify Card',
            //       tileDescription: 'Gift Card Spotify R\$20',
            //       tileType: TileType.Rewards,
            //       pointsCost: 500,
            //       alreadyTaken: 10,
            //       maxRedemptions: 10,
            //     ),
            //     SlidableTile(
            //       tileTitle: 'Nespresso',
            //       tileDescription: 'Gift 1 pack coffee Nespresso',
            //       tileType: TileType.Rewards,
            //       pointsCost: 650,
            //       alreadyTaken: 40,
            //       maxRedemptions: 40,
            //     ),
            //   ],
            // ),
          );
        },
    );
  }
}
