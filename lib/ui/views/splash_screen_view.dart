import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:gamefication/models/user_model.dart';
import 'package:gamefication/ui/controllers/versioncheckprovider.dart';
import 'package:gamefication/ui/router.dart';
import 'package:gamefication/ui/widgets/version_dialogs.dart';
import 'package:provider/provider.dart';
import 'package:gamefication/services/auth.dart';

class SplashScreenView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Future<DocumentSnapshot> auth =
        AuthenticationServices.isUserAuthenticated();

    return Consumer<VersionCheck>(
      builder: (context, VersionCheck versionCheck, _) {
        
        return Scaffold(
          body: Container(
            alignment: Alignment.center,
            child: FlareActor(
              "assets/flare/OpenLogo.flr",
              alignment: Alignment.center,
              fit: BoxFit.contain,
              animation: "Go",
              callback: (_) async {
                DocumentSnapshot snapshot = await auth;

                if (snapshot == null) {
                  Navigator.pushReplacementNamed(context, loginRoute);
                } else {                                    
                  User.email = snapshot.data['email'];
                  User.uid = snapshot.data['uid'];
                  User.displayName = snapshot.data['displayName'];
                  //TODO: Diogo: CRIAR SPLASH PROVIDER
                  Navigator.pushReplacementNamed(context, homeRoute);
                }
                if (versionCheck.getShowDialog) {
                  showVersionDialog(
                      context: context,
                      forceAsk: versionCheck.getForceAsk,
                      updateTap: versionCheck.launchURL);
                }
              },
            ),
          ),
        );
      },
    );
  }
}
