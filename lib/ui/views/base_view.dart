import 'package:flutter/material.dart';
import 'package:gamefication/ui/controllers/base_controller.dart';
import 'package:provider/provider.dart';

///encapsulate a Provider and Consumer
///
///This widget is of a given `<Controller Type>` and expects a `controller` atribute and a `builder`
class BaseView<T extends BaseController> extends StatefulWidget {
  final Widget Function(BuildContext context, T controller, Widget child) builder;
  final T controller;

  BaseView({
    Key key,
    @required this.builder,
    @required this.controller,
  }) : super(key: key);

  @override
  _BaseViewState<T> createState() => _BaseViewState<T>();
}

class _BaseViewState<T extends BaseController> extends State<BaseView<T>> {

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<T>(
      builder:(context) =>  widget.controller,
      child: Consumer<T>(builder: widget.builder),
    );
  }
}
