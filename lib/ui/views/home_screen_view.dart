import 'package:flutter/material.dart';
import 'package:gamefication/models/user_model.dart';
import 'package:gamefication/ui/controllers/base_controller.dart';
import 'package:gamefication/utils/formatters.dart';
import 'package:page_view_indicator/page_view_indicator.dart';
import 'package:gamefication/services/auth.dart';
import 'package:gamefication/models/game_model.dart';
import 'package:gamefication/ui/controllers/home_screen_controller.dart';
import 'package:gamefication/ui/router.dart';
import 'package:gamefication/ui/widgets/app_card.dart';
import 'package:gamefication/ui/widgets/app_list_tile.dart';
import 'package:gamefication/ui/widgets/backdrop.dart';
import 'package:gamefication/ui/widgets/double_string_column.dart';
import 'package:gamefication/ui/widgets/mission_progress_indicator.dart';
import 'package:gamefication/utils/app_colors.dart';
import 'package:gamefication/utils/dimensions.dart';
import 'package:provider/provider.dart';

final Formatters formatters = Formatters();

// Reference -> https://medium.com/flutter/decomposing-widgets-backdrop-b5c664fb9cf4
class HomeScreenView extends StatelessWidget {
  HomeScreenView({Key key}) : super(key: key);
//TODO: show a nice screen loader while the controller is fetching the data
  @override
  Widget build(BuildContext context) {
    final String _frontPanelClosedTitle = 'Activate';
    final frontPanelIsVisible = ValueNotifier<bool>(false);
    //HomeScreenController homeScreenController = HomeScreenController();
    return WillPopScope(
      onWillPop: () async => false,
      child: SafeArea(
        child: Backdrop(
          panelVisible: frontPanelIsVisible,
          frontLayer: FrontPanel(),
          backLayer: BackPanel(
            frontPanelIsVisible: frontPanelIsVisible,
          ),
          frontHeader: Text(_frontPanelClosedTitle),
          frontHeaderHeight: Dimensions.frontPanelHeight,
          frontPanelOpenHeightPercentage: Dimensions.frontPanelOpenPercentage,
        ),
      ),
    );
  }
}

class FrontPanel extends StatelessWidget {
  FrontPanel({Key key}) : super(key: key);

  //Front Panel Strings Defs
  final String _profilePictureImage =
      'assets/images/placeholder_profile_image.png';
  final String _rankingString = 'Ranking';
  final String _myRewardPointsString = 'My Reward Points';
  final String _missionCardTitle = 'Mission';
  final String _informationIconTootltip = 'Informations';
  final String _myRecentActivityTileText = 'My recent activity';
  final String _rewardsTileTExt = 'Rewards';
  final String _purchaseHistoryTileTExt = 'Purchase History';

  @override
  Widget build(BuildContext context) {
    return Consumer<HomeScreenController>(
      builder:
          (BuildContext context, HomeScreenController homeScreenController, _) {
        return Material(
          child: homeScreenController.state == ViewState.Busy
              ? Center(child: CircularProgressIndicator())
              : Padding(
                  padding: const EdgeInsets.only(
                    top: 8.0,
                    right: Dimensions.smallPadding,
                    left: Dimensions.smallPadding,
                  ),
                  child: ListView(
                    children: <Widget>[
                      AppCard(
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                Container(
                                  height: Dimensions.profilePictureSize,
                                  width: Dimensions.profilePictureSize,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                      fit: BoxFit.fill,
                                      image: AssetImage(_profilePictureImage),
                                    ),
                                  ),
                                ),
                                Dimensions.widthSpacer(),
                                Expanded(
                                  child: Text(
                                    User.displayName,
                                    style: Theme.of(context).textTheme.body1,
                                    overflow: TextOverflow.fade,
                                  ),
                                ),
                              ],
                            ),
                            Dimensions.heightSpacer(
                                Dimensions.smallPadding * 2),
                            Divider(
                              thickness: 1.5,
                              indent: Dimensions.smallPadding,
                              endIndent: Dimensions.smallPadding,
                            ),
                            Dimensions.heightSpacer(Dimensions.smallPadding),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Flexible(
                                    flex: 4,
                                    child: Center(
                                      child: DoubleStringColumn(
                                        string1: _rankingString,
                                        string2: homeScreenController
                                                .selectedGame.ranking
                                                .toString() +
                                            '°',
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                    flex: 1,
                                    child: Container(
                                      //Simulates a vertical divider. Idk why  the native verticalDivider doesnt work
                                      width: 2.1,
                                      height: 70,
                                      color: Theme.of(context).dividerColor,
                                    ),
                                  ),
                                  Flexible(
                                    flex: 6,
                                    child: DoubleStringColumn(
                                      string1: _myRewardPointsString,
                                      string2: formatters.intWithDot(
                                              homeScreenController.selectedGame
                                                  .currentRewardPoints) +
                                          ' / ' +
                                          formatters.intWithDot(
                                              homeScreenController.selectedGame
                                                  .totalRewardPoints),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      AppCard(
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  _missionCardTitle,
                                  style: Theme.of(context).textTheme.title,
                                ),
                                IconButton(
                                  icon: Icon(
                                    Icons.info_outline,
                                    color: Colors.black,
                                    size: 32,
                                  ),
                                  onPressed: () => null,
                                  tooltip: _informationIconTootltip,
                                ),
                              ],
                            ),
                            MissionProgressIndicator(
                              desiredProgressMarkerColor:
                                  AppColors.desiredProgressMarker,
                              goalOfTheDayMarkerColor:
                                  AppColors.goalOfTheDayMarker,
                              currentProgress: homeScreenController
                                  .selectedGame.mission.currentProgress,
                              desiredProgress: homeScreenController
                                  .selectedGame.mission.desiredProgress,
                              goalOfTheDay: homeScreenController
                                  .selectedGame.mission.goalOfTheDay,
                            ),
                          ],
                        ),
                      ),
                      Dimensions.heightSpacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,  
                        crossAxisAlignment: CrossAxisAlignment.start,                      
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(context, recentActivityRoute);
                            },
                            child: Container(
                              width: Dimensions.screenWidth(context)*0.20,
                              height: 80,
                              child: Column(
                                //mainAxisAlignment: MainAxisAlignment.center,                                
                                children: <Widget>[
                                  Icon(Icons.access_time, size: 40),
                                  Text(_myRecentActivityTileText,
                                  textAlign: TextAlign.center,
                                      style: Theme.of(context)
                                          .textTheme
                                          .subhead
                                          .copyWith(color: Colors.black))
                                ],
                              ),
                            ),
                          ),
                          Container(height: 50,
                            width: 1.5,
                            color: Colors.grey[500],
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(context, rewardsRoute);
                            },
                            child: Container(
                              width: Dimensions.screenWidth(context)*0.20,
                              height: 80,
                              child: Column(
                                //mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(Icons.card_giftcard, size: 40),
                                  Text(_rewardsTileTExt,
                                  textAlign: TextAlign.center,
                                      style: Theme.of(context)
                                          .textTheme
                                          .subhead
                                          .copyWith(color: Colors.black))
                                ],
                              ),
                            ),
                          ),
                          Container(height: 50,
                            width: 1.5,
                            color: Colors.grey[500],
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(
                                  context, purchaseHistoryRoute);
                            },
                            child: Container(
                              width: Dimensions.screenWidth(context)*0.20,
                              height: 80,
                              child: Column(
                                //mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(Icons.loyalty, size: 40),
                                  Text(_purchaseHistoryTileTExt,
                                  textAlign: TextAlign.center,
                                      style: Theme.of(context)
                                          .textTheme
                                          .subhead
                                          .copyWith(color: Colors.black))
                                ],
                              ),
                            ),
                          ),
                          Container(height: 50,
                            width: 1.5,
                            color: Colors.grey[500],
                          ),
                          GestureDetector(
                            onTap: () {
                              AuthenticationServices.userLogout();
                              Navigator.pushReplacementNamed(
                                  context, loginRoute);
                            },
                            child: Container(
                              width: Dimensions.screenWidth(context)*0.20,
                              height: 80,
                              child: Column(
                                //mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(Icons.exit_to_app, size: 40),
                                  Text('Logout',
                                  textAlign: TextAlign.center,
                                      style: Theme.of(context)
                                          .textTheme
                                          .subhead
                                          .copyWith(color: Colors.black))
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      Dimensions.heightSpacer(Dimensions.smallPadding),
                    ],
                  ),
                ),
        );
      },
    );
  }
}

class BackPanel extends StatelessWidget {
  BackPanel({
    Key key,
    @required this.frontPanelIsVisible,
  }) : super(key: key);
  final ValueNotifier<bool> frontPanelIsVisible;

  final pageIndexNotifier = ValueNotifier<int>(0);
  final String _datesConectorString = ' até ';

  Widget _showGameBanner(BuildContext context,
      ValueNotifier<bool> frontPanelIsVisible, Game game) {
    //TODO : use a layout builder to define this height, maybe put this inside the backdrop def?
    final double _bannerBottomPaddingWhenFrontPanelIsVisible =
        (Dimensions.frontPanelOpenPercentage *
                Dimensions.screenHeight(context)) -
            Dimensions.screenHeight(context) * 0.25;
    return ValueListenableBuilder(
      valueListenable: frontPanelIsVisible,
      builder: (context, _frontPanelIsVisible, _) {
        return AnimatedContainer(
          duration: Duration(milliseconds: 250),
          curve: Curves.linearToEaseOut,
          margin: frontPanelIsVisible.value
              ? EdgeInsets.only(
                  bottom: _bannerBottomPaddingWhenFrontPanelIsVisible)
              : const EdgeInsets.only(
                  top: 60.0,
                  left: Dimensions.bigPadding,
                  right: Dimensions.bigPadding,
                ),
          decoration: BoxDecoration(
            color: Colors.white70,
            image: DecorationImage(
              fit: BoxFit.fitWidth,
              image: AssetImage(game.imagePath),
            ),
          ),
          child: Column(
            children: <Widget>[
              Dimensions.heightSpacer(Dimensions.mediumPadding),
              Text(
                game.name,
                style: Theme.of(context).textTheme.display1,
              ),
              Dimensions.heightSpacer(Dimensions.smallPadding),
              if (!_frontPanelIsVisible)
                Text(
                  formatters.date(game.startDate) +
                      _datesConectorString +
                      formatters.date(game.endDate),
                  style: Theme.of(context).textTheme.subhead,
                ),
              // RaisedButton(
              //   child: Text('logout'),
              //   onPressed: () async {
              //     await AuthenticationServices.userLogout();
              //     Navigator.pushNamed(context, loginRoute);
              //   },
              // ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<HomeScreenController>(
      builder: (context, HomeScreenController homeScreenController, _) {
        return Scaffold(
          body: homeScreenController.state == ViewState.Busy
              ? Center(child: CircularProgressIndicator())
              : Column(
                  children: <Widget>[
                    Flexible(
                      child: PageView.builder(
                        onPageChanged: (index) {
                          pageIndexNotifier.value = index;
                          homeScreenController.setSelectedGame(index);
                        },
                        itemCount: 5,
                        itemBuilder: (context, index) {
                          return _showGameBanner(context, frontPanelIsVisible,
                              homeScreenController.selectedGame);
                        },
                      ),
                    ),
                    Dimensions.heightSpacer(Dimensions.smallPadding),
                    PageViewIndicator(
                      pageIndexNotifier: pageIndexNotifier,
                      length: 5,
                      normalBuilder: (animationController, index) => Circle(
                        size: 8.0,
                        color: AppColors.pageViewUnselectedIndicator,
                      ),
                      highlightedBuilder: (animationController, index) =>
                          ScaleTransition(
                        scale: CurvedAnimation(
                          parent: animationController,
                          curve: Curves.ease,
                        ),
                        child: Circle(
                          size: 12.0,
                          color: AppColors.pageViewSelectedIndicator,
                        ),
                      ),
                    ),
                    Dimensions.heightSpacer(Dimensions.mediumPadding),
                  ],
                ),
        );
      },
    );
  }
}
