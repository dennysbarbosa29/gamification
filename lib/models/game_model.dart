import 'package:gamefication/models/challenges_model.dart';
import 'package:gamefication/models/purchase_history_model.dart';
import 'package:gamefication/utils/formatters.dart';
import 'package:meta/meta.dart';
import 'package:gamefication/models/reward_model.dart';

class Game {
  Game({
    @required this.name,
    this.imagePath = 'assets/images/open_labs_black.png', //Implement this
    @required this.ranking,
    @required this.currentRewardPoints,
    @required this.totalRewardPoints,
    @required this.mission,
    @required this.startDate,
    @required this.endDate,
    @required this.rewardsMap,

    /// Map<String rewardName, Reward reward>
    @required this.purchaseHistory,

    /// Map<DateTime date, PurchaseHistory purchase>
    @required this.challenges,

    /// Map<String(?) challengeType, Challenge challenge>
  })  : assert(name != null && name.isNotEmpty),
        assert(ranking != null && ranking.isFinite && !ranking.isNegative),
        assert(currentRewardPoints != null &&
            currentRewardPoints.isFinite &&
            !currentRewardPoints.isNegative),
        assert(totalRewardPoints != null &&
            totalRewardPoints.isFinite &&
            !totalRewardPoints.isNegative),
        assert(mission != null),
        assert(startDate != null),
        assert(endDate != null) {
    // Contrstuctor Body below
    if (rewardsMap != null && rewardsMap.isNotEmpty) _initRewardsStatuses();
    _sortPurchaseHistory();
  }

  factory Game.fromJson(Map<String, dynamic> json) {
    Map<String, Reward> _rewardsMapFromJson = {};
    Map<DateTime, PurchaseHistory> _purchaseHistoryFromJson = {};
    Map<String, Challenge> _challengesFromJson = {};

    // Populate Rewards Map
    if (json['Rewards'] != null)
    for (Map<String, dynamic> reward in json['Rewards'])
      _rewardsMapFromJson[reward['name']] = Reward.fromJson(reward);

    //Populate Purchase history Map
    if (json['purchaseHistory'] != null)
    for (Map<String, dynamic> purchase in json['purchaseHistory'])
      _purchaseHistoryFromJson[DateTime.parse(purchase['date'])] =
          PurchaseHistory.fromJson(purchase);

    //Populate Challenges Map
    if (json['challenges'] != null)
    for (Map<String, dynamic> challenge in json['challenges'])
      _challengesFromJson[challenge['price']] = Challenge.fromJson(challenge); //maybe change this key.(add challengeType to JSON?)

    return Game(
      name: json['name'],
      ranking: int.parse(json['ranking']),
      imagePath: json['imagePath'],
      currentRewardPoints: int.parse(json['currentRewardPoints']),
      totalRewardPoints: int.parse(json['totalRewardPoints']),
      mission: Mission.fromJson(json['mission']),
      startDate: DateTime.parse(json['startDate']),
      endDate: DateTime.parse(json['endDate']),
      rewardsMap: _rewardsMapFromJson,
      purchaseHistory: _purchaseHistoryFromJson,
      challenges: _challengesFromJson,
    );
  }

  final String name;
  final String imagePath;
  DateTime startDate;
  DateTime endDate;
  int ranking;
  int currentRewardPoints;
  int totalRewardPoints;
  Mission mission;
  Map<String, Reward> rewardsMap;
  Map<DateTime, PurchaseHistory> purchaseHistory;
  Map<String, Challenge> challenges;

  void _initRewardsStatuses() {
    this.rewardsMap.forEach((key, value) {
      value.setRewardStatus(this.currentRewardPoints);
    });
  }

  // sort purchase history map based on map key (date), from newest to oldest date
  void _sortPurchaseHistory() {
    Map<DateTime, PurchaseHistory> sortedMap = Map.fromEntries(
        purchaseHistory.entries.toList()
          ..sort((e1, e2) => e1.key.compareTo(e2.key)));
    purchaseHistory = sortedMap;
  }

  void updateRewardStatus(String rewardKey) {
    this.rewardsMap[rewardKey].setRewardStatus(this.currentRewardPoints);
  }

  /// returns the purchase history as a `List[String date, String rewardName]`
  ///
  /// if `ascending = true` returns a list from newest date to oldest date
  ///
  /// if `ascending = false` returns a list from oldest to newest date
  // List<List<String>> getSortedPurchaseList([bool ascending = true]) {
  //   List<List<String>> _sortedList = [];
  //   List<DateTime> datesList = this.purchaseHistory.keys.toList()..sort();
  //   if (ascending = false) datesList = datesList.reversed;
  //   for (DateTime date in datesList) {
  //     _sortedList.add([Formatters().date(date), purchaseHistory[date]]);
  //     print(_sortedList);
  //   }
  //   return _sortedList;
  // }

  @override
  String toString() {
    return 'Game $name \nRanking: $ranking\nRewardPoints / Total:\n$currentRewardPoints / $totalRewardPoints';
  }
}

class Mission {
  Mission({
    @required this.currentProgress,
    @required this.desiredProgress,
    @required this.goalOfTheDay,
  })  : assert(currentProgress != null &&
            currentProgress.isFinite &&
            !currentProgress.isNegative),
        assert(desiredProgress != null &&
            desiredProgress.isFinite &&
            !desiredProgress.isNegative),
        assert(goalOfTheDay != null &&
            goalOfTheDay.isFinite &&
            !goalOfTheDay.isNegative);

  factory Mission.fromJson(Map<String, dynamic> json) {
    return Mission(
      currentProgress: double.parse(json['currentProgress']),
      desiredProgress: double.parse(json['desiredProgress']),
      goalOfTheDay: double.parse(json['goalOfTheDay']),
    );
  }

  double currentProgress;
  double desiredProgress;
  double goalOfTheDay;

  void update() {
    //GET THE NEW PROGRESSES;
  }

  @override
  String toString() {
    return 'Current Mission :\nCurrent Progress: $currentProgress\nDesired Progress: $desiredProgress\nGoal of the Day: \n$goalOfTheDay';
  }
}
