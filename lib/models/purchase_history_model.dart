import 'package:meta/meta.dart';

class PurchaseHistory {
  PurchaseHistory({
    @required this.name,
    @required this.description,
    @required this.price,
    @required this.date,
  })  : assert(name != null && name.isNotEmpty),
        assert(description != null && description.isNotEmpty),
        assert(price != null && price.isFinite && !price.isNegative),
        assert(date != null);

  factory PurchaseHistory.fromJson(Map<String, dynamic> json) {
    return PurchaseHistory(
      name: json['name'],
      description: json['description'],
      price: int.parse(json['price']),
      date: DateTime.parse(json['date']),
    );//cascade operator
  }

  final String name;
  final String description;
  final int price;
  final DateTime date;
  
  @override
  String toString() {
    return 'Date : $name | Price : $price';
  }
}
