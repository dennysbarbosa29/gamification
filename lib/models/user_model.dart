class User {

  //Mock do nome da empresa
  static const String empresa = "empresa";

  //Mock departamento
  static const String departamento = "TI";
  
  static String uid;
  static String displayName;
  static String phoneNumber;
  static String email;
  static String photoUrl;
}
