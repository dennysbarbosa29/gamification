import 'package:meta/meta.dart';

enum RewardStatus { canBuy, notEnoughtPoints, maxLimitReached }

class Reward {
  Reward({
    @required this.name,
    @required this.description,
    @required this.price,
    @required this.maxRedemptions,
    @required this.currentRedemptions,
  })  : assert(name != null && name.isNotEmpty),
        assert(description != null && description.isNotEmpty),
        assert(price != null && price.isFinite && !price.isNegative),
        assert(maxRedemptions != null &&
            maxRedemptions.isFinite &&
            !maxRedemptions.isNegative),
        assert(currentRedemptions != null &&
            currentRedemptions.isFinite &&
            !currentRedemptions.isNegative &&
            currentRedemptions <= maxRedemptions);

  factory Reward.fromJson(Map<String, dynamic> json) {
    return Reward(
      name: json['name'],
      description: json['description'],
      price: int.parse(json['price']),
      maxRedemptions: int.parse(json['maxRedemptions']),
      currentRedemptions: int.parse(json['currentRedemptions']),
    ); //cascade operator
  }

  final String name;
  final String description;
  final int price;
  final int maxRedemptions;
  int currentRedemptions;
  RewardStatus _rewardStatus;

  RewardStatus get rewardStatus => _rewardStatus;

  void setRewardStatus(int availablePoints) {
    if (this.maxRedemptions == this.currentRedemptions)
      _rewardStatus = RewardStatus.maxLimitReached;
    else if (availablePoints >= this.price)
      _rewardStatus = RewardStatus.canBuy;
    else
      _rewardStatus = RewardStatus.notEnoughtPoints;
  }

  @override
  String toString() {
    return 'Reward : $name | Price : $price | Redemptions : $currentRedemptions/$maxRedemptions | Status : $_rewardStatus';
  }
}
