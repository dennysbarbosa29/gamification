import 'package:flutter/services.dart';
import 'dart:convert';
import 'package:gamefication/models/game_model.dart';

abstract class IGameFetcher {
  Future<String> _fetchData();
  Future<List<Game>> getGamesList();
//  Game getSingleGame(String gameName);
  bool shouldUpdateData();
}

class JsonGameFetcherAdapter implements IGameFetcher {
  //static const int _numberOfGames = 5;

  @override
  Future<String> _fetchData() async {
    //TODO: change this to dart file lib or create a widget test???
    return await rootBundle.loadString('assets/jsons/games.json');
  }

  @override
  Future<List<Game>> getGamesList() async {
    List<Game> _gameList = [];
    Map<String, dynamic> _parsedJson = json.decode(await _fetchData());
    for (Map<String, dynamic> gameJson in _parsedJson['Games']) {
      _gameList.add(Game.fromJson(gameJson));
    }
    return _gameList;
  }

  @override
  bool shouldUpdateData() {
    return false;
  }
}
