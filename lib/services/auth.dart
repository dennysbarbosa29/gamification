import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:gamefication/main.dart';
import 'package:gamefication/models/user_model.dart';
import 'package:gamefication/services/firebase_database.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AuthenticationResult {
  bool status;
  String errorCode;
  String errorMessage;
}

class AuthenticationServices {
  static Future<DocumentSnapshot> isUserAuthenticated() async {
    DocumentSnapshot document;

    FirebaseUser firebaseUser = await FirebaseAuth.instance.currentUser();

    if (firebaseUser == null) {
      return null;
    } else {
      User.uid = firebaseUser.uid;
      print(User.uid);
      document = await FirestoreHandler.getUser(
          departamento: User.departamento, empresa: User.empresa);
    }

    return document;
  }

  ///Atualiza as informações do usuário [displayName]
  static void _updateUserInfo(FirebaseUser user) async {
    UserUpdateInfo updateInfo = UserUpdateInfo();
    updateInfo.displayName = User.displayName;
    await user.updateProfile(updateInfo);
  }

  static Future<AuthenticationResult> emailSingUp(String password) async {
    AuthenticationResult authResult = AuthenticationResult();

    try {
      FirebaseUser user =
          (await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: User.email,
        password: password,
      ))
              .user;

      //Salva o uid gerado
      User.uid = user.uid;

      _updateUserInfo(user);

      //Adiciona o usuário no banco de dados
      FirestoreHandler.addUser(
          empresa: User.empresa, departamento: User.departamento);

      authResult.status = true;
      return authResult;
    } on PlatformException catch (error) {
      authResult.errorCode = error.code;
      authResult.errorMessage = error.message;
      authResult.status = false;
      return authResult;
    }
  }

  static Future<AuthenticationResult> emailSingIn(String password) async {
    AuthenticationResult authResult = AuthenticationResult();

    try {
      FirebaseUser user =
          (await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: User.email,
        password: password,
      ))
              .user;

      //TODO : pegar dados do usuário do database !!
      User.uid = user.uid;
      User.displayName = user.displayName;
      authResult.status = true;
      return authResult;
    } on PlatformException catch (error) {
      authResult.errorCode = error.code;
      authResult.errorMessage = error.message;
      authResult.status = false;
      return authResult;
    }
  }

  static Future<void> userLogout() async {
    await FirebaseAuth.instance.signOut();

    await FacebookLogin().logOut();

    try {
      await GoogleSignIn().disconnect();
    } catch (e) {}

    main();
  }
}
